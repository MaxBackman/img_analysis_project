import argparse
import sys
import scipy
import math
import os
import numpy as np
import re
import pandas as pd
import datetime
from scipy.stats.stats import spearmanr



id_list = []
marker_plur = ["141Pr-alphaSMA(Pr141Di)", "170Er-CD3(Er170Di)", "162Dy-CD8a(Dy162Di)", "171Yb-CD44(Yb171Di)",
               "150Nd-PD-L1(Nd150Di)",
               "164Dy-PD-1(Dy164Di)", "147Sm-Beta-catenin(Sm147Di)", "172Yb-FoxP3(Yb172Di)", "168Er-Ki67(Er168Di)",
               "143Nd-Vimentin(Nd143Di)"]

df = pd.DataFrame(columns=marker_plur)
results = pd.DataFrame(columns=["marker", "rho", "p-value"])


def pathing_img():

    """" A highly specialized function for finding relevant IDs and their relative paths for further analysis """

    pth_plur = []
    id_plur = []

    for dirpath, dirnames, filenames in os.walk('data', topdown=True):

        for subdirname in dirnames:

            # we want to extract ID and path to the text files from "panel 1" subdirectories

            if bool(re.findall("panel\s[^2].+\w{3,5}\s(\d{3})", os.path.join(dirpath, subdirname))):  # extract the dirs

                # print(os.path.join(dirpath, subdirname))  # path to the dirs we want
                # print(re.findall("\w{3,5}\s([0-9]{3})", os.path.join(dirpath, subdirname)))  # extract core ID from path
                id = re.findall("\w{3,5}\s([0-9]{3})", os.path.join(dirpath, subdirname))

                for file in os.listdir(os.path.join(dirpath, subdirname)):
                    if bool(re.findall(".txt$", file)):
                        # print(file)
                        text_file = file

                # print(os.path.join(dirpath, subdirname, text_file))
                pth = os.path.join(dirpath, subdirname, text_file)  # copy the "full" relative path for the .txt-file

                pth_plur.append(pth)
                id_plur.append(id)



        # print(list(zip(id_plur, pth_plur)))  # in printing the escape '\'s will be excluded

    return list(zip(id_plur, pth_plur)), id_plur  # return a list of tuples with id and path for each core

def pathing_annot():

    """" A function for finding the relative paths of the markers """

    pth_plur = []
    marker_plur = []

    for dirpath, dirnames, filenames in os.walk('data/annotations/', topdown=True):

        for file in filenames:

            # print(os.path.join(dirpath, file))
            # print(file)
            # print(re.findall("(\w+)_lungcascreen\.csv$", file))

            pth = os.path.join(dirpath, file)
            marker = re.findall("(\w+)_lungcascreen\.csv$", file)

            pth_plur.append(pth)
            marker_plur.append(marker)

        # print(list(zip(marker_plur, pth_plur)))  # in printing the escape '\'s will be excluded

    return list(zip(marker_plur, pth_plur))  # return a list of tuples with id and path for each core


def read_img(__input__):

    """" Taking paths from pathing_img() and reading the files, compiling them into a pandas dataframe of markers and summed
    read-off scores. Takes a list of tuples of IDs and pathnames as input"""



    for id_pth_plur in __input__:

        my_file = pd.read_csv(id_pth_plur[1], delimiter = "\t")

        # my_file = pd.read_csv(pathing_img()[0][0][1], delimiter="\t")  # Pandas is much faster at loading csvs
        # my_file = pd.read_csv("data/test.txt", delimiter="\t")  # Pandas is much faster at loading csvs

        my_file_cols = list(my_file.columns.values)
        my_file = my_file.as_matrix(columns=None)  # convert to a NumPy-array

        my_file_sums = []

        for col in list(range(my_file.shape[1])):  # adding the sums of the columns to a list
            my_file_sums.append(my_file[:, col].sum())

        my_dict = dict(zip(my_file_cols, my_file_sums))  # creating a dictionary of tuples with header and the column data


        img_readoffs = []

        for marker in marker_plur:

            print(marker)
            print(my_dict.get(marker))
            img_readoffs.append(my_dict.get(marker))

        img_readoffs = np.asarray(img_readoffs)  # make a np.array to add as a row to df
        df.loc[len(df)] = img_readoffs

    # df.iloc[:, 0] = pathing_img()[1]
    df.insert(0, "ids", [''.join(x) for x in pathing_img()[1]])  # inserting a column of corresponding IDs
    img_df = df.iloc[df.iloc[:, 0].argsort]  # sorting pandas dataframe

    return img_df, id_list


def read_annot(__input__):

    """ Taking paths from pathing_annot() and reading the files, compiling them into a pandas dataframe of markers and
    annotation scores """


    # for i in pathing_img():  # get the IDs from the pathing
    #     id = i[0][0]
    #     id_list.append(id)

    id_list = pathing_img()[1]

    annot_readoffs = np.asarray(id_list)  # create an ndarray with IDs to later add on annotations from column 8 of the csvs
    annot_readoffs = annot_readoffs[annot_readoffs[:,0].argsort()] # sorting IDs

    for marker_pth_plur in __input__:  # marker_pth_plur are the elements of pathing_annot() here

        my_file = pd.read_csv(marker_pth_plur[1], delimiter = ",", header=None)

        # my_file = pd.read_csv(pathing_annot()[1][1], delimiter = ",", header=None)  # selecing the path to the marker-csv file

        my_file = my_file.round(decimals=2)  # remove headers and round to 2 decimals


        my_file = my_file.as_matrix(columns=None)  # convert to a NumPy-array
        my_file = my_file[1:,:]

        keep_row_plur = np.asarray(pathing_img()[1])  # the IDs we want from the annotation csv-files

        del_row_plur = []

        for del_row in range(0, my_file.shape[0]):  # keep only rows that correspond to the IDs from the imgage readoffs

            if str(my_file[del_row, 0]) not in keep_row_plur:

                del_row_plur.append(del_row)


        my_file = np.delete(my_file, del_row_plur, 0)

        my_file = my_file[my_file[:, 0].argsort()]

        annot_readoffs = np.c_[annot_readoffs, my_file[0:my_file.shape[0], 8]]


        # annot_readoffs[annot_readoffs.argsort()] == my_file[:, 0]  # checking whether IDs align


    return annot_readoffs




if __name__ == "__main__":

    """ Main function to call other functions. Compare manual annotations with image readoffs by laser """


    img_df = read_img(pathing_img()[0])[0]

    annot_df = read_annot(pathing_annot())

    # if img_df.iloc[:, 0] == annot_df[:, 0]:

    for i in range(1, img_df.shape[1]):
        a = annot_df[:, i]
        b = img_df.iloc[:, i]

        marker = marker_plur[i-1]
        rho = spearmanr(a, b)[0]  # correlation coefficient
        pval = spearmanr(a, b)[1]  # p-value

        results.loc[len(results)] = [marker, rho, pval]


        # Writing results to csv

        now = datetime.datetime.now()  # date for output
        results_path = "results/" + str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "/"  # path for results

        if os.path.exists(results_path):
            results.to_csv(path_or_buf=results_path + "results.csv")  # writing results to a csv-file
            img_df.to_csv(path_or_buf=results_path + "img_df.csv")  # writing results to a csv-file

        else:
            os.makedirs(results_path)
            results.to_csv(path_or_buf=results_path + "results.csv")  # writing results to a csv-file
            img_df.to_csv(path_or_buf=results_path + "img_df.csv")  # writing results to a csv-file



        # temp = pd.read_csv(filepath_or_buffer=results_path + "img_df.csv", delimiter=",")  # read csv to df





